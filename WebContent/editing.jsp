<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー編集画面</title>
		<link href="./css/editingStyle.css" rel="stylesheet" type="text/css">
	</head>
	<!--<script type="text/javascript">
		function functionName1(id){
			var select1 = document.forms.functionName1.branchId;
			var select2 = document.forms.functionName1.positionId;

			if(select1.options[select1.selectedIndex].value == 1){
				if(id == 1){
					select2.options[0] = new Option("総務人事", 1, true);
					select2.options[1] = new Option("情報管理", 2);
				} else if(id == 2){
					select2.options[0] = new Option("総務人事", 1);
					select2.options[1] = new Option("情報管理", 2, true);
				}
			} else if(select1.options[select1.selectedIndex].value == 2){
				if(id == 3){
					select2.options[0] = new Option("支店長", 3, true);
					select2.options[1] = new Option("社員", 4);
				} else if(id == 4){
					select2.options[0] = new Option("支店長", 3);
					select2.options[1] = new Option("社員", 4, true);
				}
			}
		}
	</script>-->
	<%-- <body onLoad="functionName1(${ afterEditing.positionId })"> --%>
	<body>
		<div class="main-contents">
			<div class="name">ユーザー編集</div>
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${ errorMessages }" var="message">
							<li><c:out value="${ message }" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<div class="container">

				<div class="header">
					<a href="./management" id="management_btn">ユーザー管理へ戻る</a><br /><br />
					<a href="./" id="home_btn">ホームへ</a>
				</div>

				<div class="main">
					<form name="functionName1" action="editing" method="post">
						<input type="hidden" name="editingId" value="${ editing.id }">
						<label for="">ログインID (半角英数字:6文字以上20文字以内)</label><br />
						<input name="loginId" value="${ afterEditing.loginId }" id="loginId" /><br />
						<label for="">パスワード (半角文字:6文字以上20文字以内)</label><br />
						<input name="password" type="password" id="password" /><br />
						<label for="">パスワード(確認用 :上記と同じ)</label><br />
						<input name="password2" type="password" id="password2" /><br />
						<label for="">ユーザー名 (10文字以下)</label><br />
						<input name="userName" value="${ afterEditing.userName }" id="userName" /><br /><br />
						<c:if test="${ loginUser.id != editing.id }">
							支店名
							<select name="branchId"><%-- onChange="functionName1(${ afterEditing.positionId })">--%>
								<c:forEach items="${ branches }" var="branch">
									<c:if test="${ branch.id == afterEditing.branchId }">
										<option value="${ branch.id }" selected>${ branch.branchName }</option>
									</c:if>
									<c:if test="${ branch.id != afterEditing.branchId }">
										<option value="${ branch.id }">${ branch.branchName }</option>
									</c:if>
								</c:forEach>
							</select>
							役職・部署名
							<select name="positionId">
								<c:forEach items="${ positions }" var="position">
									<c:if test="${ position.id == afterEditing.positionId }">
										<option value="${ position.id }" selected>${ position.positionName }</option>
									</c:if>
									<c:if test="${ position.id != afterEditing.positionId }">
										<option value="${ position.id }">${ position.positionName }</option>
									</c:if>
								</c:forEach>
							</select>
						</c:if>
						<br />
						<br />
						<input type="submit" value="編集確定" id="editing_btn" /><br />
					</form>
				</div>

			</div>
			<c:remove var="afterEditing" scope="session" />
			<div class="copyright">Copyright(c)HosoiKota</div>
		</div>
	</body>
</html>