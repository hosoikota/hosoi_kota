<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ホーム画面</title>
		<link href="./css/homeStyle.css" rel="stylesheet" type="text/css">
		<script>
			function check1(){
				if(window.confirm('この投稿を削除してもよろしいですか？')){
					return true;
				} else {
					return false;
				}
			}
		</script>
		<script>
			function check2(){
				if(window.confirm('このコメント削除してもよろしいですか？')){
					return true;
				} else {
					return false;
				}
			}
		</script>
	</head>
	<body>
		<div class="wrapper">
			<div class="name"><h2><c:out value="${ loginUser.userName }の掲示板" /></h2></div>
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${ errorMessages }" var="message">
							<li id="error"><c:out value="${ message }"/>
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session"/>
			</c:if>
			<div class="container">
				<div class="side">
					<div class="link">
						<c:if test="${ not empty loginUser }">
							<a href="contribution" class="contribution_btn">新規投稿</a><br /><br />
							<c:if test="${ loginUser.positionId == 1 && loginUser.branchId == 1 }">
								<a href="management" class="management_btn">ユーザー管理画面</a><br /><br />
							</c:if>
						</c:if>
					</div>
					<div class="retrieval">
						<form action="./" method="get">
							<label for="date">投稿日時</label><br />
							<input type ="date" value="${ datestart }" name="datestart"> から<br />
							<div class="bou">|</div>
							<input type ="date" value="${ dateend }" name="dateend"> まで<br />
							<br />
							<label for="text">カテゴリー (10文字以下)</label><br />
							<input name="category" value="${ category }" id="category"><br />
							<%--<c:remove var="category"/>--%>
							<div class="hoge1_btn"><input type="submit" value="検索" id="kennsaku"></div>
						</form>
						<form action="./" method="get">
							<input type="hidden" name="category" value="${ null }">
							<div class="hoge2_btn"><input type="submit" value="リセット" id="reset"></div>
						</form>
					</div>
					<br /><br />
					<div class="hoge2_btn"><a href="logout" class="logout_btn">ログアウト</a></div><br />
				</div>
				<div class="main">
					<div class="contributions">
						<c:forEach items="${ contributions }" var="contribution">
							<div class="contribution">
								<div class="d_subject"><span class="s_subject"><c:out value="${ contribution.subject }" /></span></div>
								<div class="category_name">
									<span class="category">category: <c:out value="${ contribution.category }" /></span>
									<span class="user"> / name: <c:out value="${ contribution.userName }" /></span>
								</div>
								<div class="text"><c:out value="${ contribution.text }" /></div>
								<div class="date">投稿日時:<fmt:formatDate value="${contribution.createDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
								<c:if test="${ loginUser.id == contribution.userId }">
									<form action="contributiondelete" method="post" onsubmit="return check1()">
										<input type="hidden" name="contributionId" value="${ contribution.id }">
										<div class="delete_btn"><input type="submit" value="投稿削除"  id="contribution_delete"/></div>
									</form>
								</c:if>
							</div><br />

							<div class="userComments">
								<c:forEach items="${ comments }" var="comment">
									<c:if test="${ comment.contributionId == contribution.id }">
										<div class="comment_text">
											<c:forEach var="str" items="${ fn:split(comment.comment, '
												') }" >
												<span class="text"><c:out value="${ str }"></c:out></span><br />
											</c:forEach>
										</div>
										<br />
										<span class="user">name: <c:out value="${ comment.userName }"></c:out></span><br />
										<div class="date">投稿日時:<fmt:formatDate value="${ comment.createDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
										<c:if test="${ loginUser.id == comment.userId }">
											<form action="./" method="post" onsubmit="return check2()" id="comment_delete">
												<input type="hidden" name="commentId" value="${ comment.id }">
												<input type="submit" value="コメント削除"  id="comment_delete_btn"/><br />
											</form>
										</c:if>
									</c:if>
								</c:forEach>
							</div>

							<div class="comments">
								<form action="./" method="post">
									<label for="text">コメント(500文字以内)</label><br />
									<textarea name="text" rows="5" cols="50"><c:if test="${ contribution.id == id }"><c:out value="${ text }"/></c:if></textarea><br />
									<input type="hidden" name="userId" value="${ loginUser.id }">
									<input type="hidden" name="contributionId" value="${ contribution.id }">
									<input type="submit" value="コメント投稿"  id="comment_btn"/><br />
								</form>
							</div>
						</c:forEach>
						<c:remove var="text" scope="session"/>
					</div>
				</div>
			</div>
			<div class="copyright">Copyright(c)HosoiKota</div>
		</div>
	</body>
</html>