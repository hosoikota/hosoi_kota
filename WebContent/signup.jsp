<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー新規登録画面</title>
		<link href="./css/signupStyle.css" rel="stylesheet" type="text/css">
		<script type="text/javascript">
			function functionName(){
				var select1 = document.forms.formName.branch_id;
				var select2 = document.forms.formName.position_id;
				//select2.options.lenght = 0;

				if(select1.options[select1.selectedIndex].value == 1){
					select2.options[0] = new Option("総務人事", 1);
					select2.options[1] = new Option("情報管理", 2);
				} else if(select1.options[select1.selectedIndex].value == 2){
					select2.options[0] = new Option("支店長", 3);
					select2.options[1] = new Option("社員", 4);
				}
			}
		</script>
	</head>
	<body onLoad="functionName()">
		<div class="main-contents">
			<div class="header">ユーザー新規登録</div>
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${ errorMessages }" var="message">
							<li><c:out value="${ message }" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
			<div class="container">
				<div class="side">
					<a href="./management" class="link1">ユーザー管理画面へ</a><br /> <br />
					<a href="./" class="link2">ホームへ</a>
				</div>
				<div class="main">
				<form name="formName" action="signup" method="post"><br />
					<label for="login_id">ログインID (半角英数字:6文字以上20文字以内)</label><br />
					<input type="text" name="login_id" value="${ user.loginId }" id="login_id" /><br />
					<label for="password">パスワード (半角文字:6文字以上20文字以内)</label><br />
					<input type="password" name="password" id="password" /><br />
					<label for="password">パスワード（確認用:上記と同じ）</label><br />
					<input type="password" name="password2" id="password2" /><br />
					<label for="user_name">ユーザー名 (10文字以下)</label><br />
					<input type="text" name="user_name" value="${ user.userName }" id="user_name" /><br /><br />
					支店名
					<select name="branch_id" onChange="functionName()">
						<option value=1>本社</option>
						<option value=2>支店A</option>
					</select>
					役職・部署名
					<select name="position_id">
					</select>
					<c:remove var="user" scope="session" />
					<br /><br /><input type="submit" value="登録" id="touroku_btn"/><br />
				</form>
				</div>
			</div>
			<div class="footer">Copyright(c)HosoiKota</div>

		</div>
	</body>
</html>