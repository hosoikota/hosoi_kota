<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー管理画面</title>
		<link href="./css/managementStyle.css" rel="stylesheet" type="text/css">
		<script>
			function check1(name) {
				if(window.confirm('「 ' + name + ' 」を停止してもよろしいですか？')){
					return true;
				} else {
					return false;
				}
			}
		</script>
		<script>
			function check2(name) {
				if(window.confirm('「 ' + name + ' 」を復活してもよろしいですか？')){
					return true;
				} else {
					return false;
				}
			}
		</script>
	</head>
	<body>
		<div class="main-contents">
			<div class="name">ユーザー管理</div>
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${ errorMessages }" var="message">
							<li id="error"><c:out value="${ message }" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
			<div class="container">
				<div class="header">
					<a href="signup" class="signup_btn">ユーザー新規登録</a><br /><br />
					<a href="./" class="home_btn">ホーム画面へ</a><br />
				</div>
				<div class="user-management">
						<table class="table">
							<tr><th>ID</th><th>ログインID</th><th>ユーザー名</th><th>支店名</th><th>	部署・役職</th><th>機能情報</th><th>編集</th></tr>
							<c:forEach items="${ userManagements }" var="user">
							<tr><th>${ user.id }</th><th>${ user.loginId }</th><th>${ user.userName }</th><th>${ user.branchName }</th><th>${ user.positionName }</th>
								<th>
									<c:if test="${ nowUser.id == user.id }">
										ログイン中
									</c:if>
									<c:if test="${ nowUser.id != user.id && user.isStoped == 0 }">
										<form action="management" method="post" onsubmit="return check1('${ user.userName }')">
											<input type="hidden" name="userId" value="${ user.id }" />
											<input type="hidden" name="isStoped" value="1" />
											<input type="submit" value="アカウント停止" id="stop_btn" />
										</form>
									</c:if>
									<c:if test="${ nowUser.id != user.id && user.isStoped == 1 }">
										<form action="management" method="post" onsubmit="return check2('${ user.userName }')">
											<input type="hidden" name="userId" value="${ user.id }" />
											<input type="hidden" name="isStoped" value="0" />
											<input type="submit" value="アカウント復活" id="start_btn">
										</form>
									</c:if>
								</th>
								<th>
								<form action="editing" method="get">
									<input type="hidden" name="editingId" value="${ user.id }" />
									<input type="submit" value="編集" id="management_btn" />
								</form>
								</th>
							</tr>
							</c:forEach>
						</table>
				</div>
			</div>
			<div class="copyright">Copyright(c)HosoiKota</div>
		</div>
	</body>
</html>