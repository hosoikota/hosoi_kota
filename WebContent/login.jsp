<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ログイン画面</title>
		<link href="./css/loginStyle.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="main-conntents">

			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${ errorMessages }" var="message">
							<li id="error"><c:out value="${ message }"/>
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session"/>
			</c:if>

			<form action="login" method="post">
				<label for="loginId">ログインID</label><br />
				<input name="loginId" value="${ id }" id="loginId"><br />
				<c:remove var="id" scope="session" />

				<label for="password">パスワード</label><br />
				<input name="password" type="password" id="password"></input><br />

				<input type="submit" value="ログイン" id="login_btn" /><br />
			</form>
			<div class="copyright">Copyright(c)HosoiKota</div>
		</div>
	</body>
</html>