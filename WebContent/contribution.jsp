<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>新規投稿画面</title>
		<link href="./css/contributionStyle.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="contribution-area">
			<div class="name">新規投稿</div>
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${ errorMessages }" var="message">
							<li><c:out value="${ message }"/>
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session"/>
			</c:if>
			<div class="container">
			<div class="header">
				<a href="./" id="home_btn">ホームへ</a>
			</div>
			<div class="main">
			<form action="contribution" method="post">
				<label for="subject">件名 (30文字以下)</label><br />
				<input type="text" name="subject" value="${ contribution.subject }" id="subject"><br />

				<label for="category">カテゴリー (10文字以下)</label><br />
				<input type="text" name="category" value="${ contribution.category }" id="category"><br />

				<label for="text">本文 (1000文字以下)</label><br />
				<textarea name="text" rows="5" cols="50">${ contribution.text }</textarea><br />
				<c:remove var="contribution" scope="session"/>
				<input type="submit" value="投稿" id="contribution_btn" />
			</form>
			</div>
			</div>

			<div class="copyright">Copyright(c)HosoiKota</div>

		</div>

	</body>
</html>