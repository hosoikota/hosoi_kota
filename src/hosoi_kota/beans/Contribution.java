package hosoi_kota.beans;

import java.io.Serializable;
import java.util.Date;

public class Contribution implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String subject; //件名
	private String text; //本文
	private String category; //カテゴリー
	private int userId; //ユーザーtableのid
	private Date createDate; //登録日時

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


}
