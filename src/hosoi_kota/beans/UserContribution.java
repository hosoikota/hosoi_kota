package hosoi_kota.beans;

import java.io.Serializable;
import java.util.Date;

public class UserContribution implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id; //投稿番号かな
	private String subject; //件名
	private String category; //カテゴリー
	private String text; //本文
	private String userName; //投稿したユーザー名
	private int userId;
	private Date createDate;



	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}




}
