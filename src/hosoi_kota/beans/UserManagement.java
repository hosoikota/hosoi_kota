package hosoi_kota.beans;

import java.io.Serializable;

public class UserManagement implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String loginId;
	private String userName;
	private String branchName;
	private String positionName;
	private int isStoped;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getPositionName() {
		return positionName;
	}
	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}
	public int getIsStoped() {
		return isStoped;
	}
	public void setIsStoped(int isStoped) {
		this.isStoped = isStoped;
	}



}
