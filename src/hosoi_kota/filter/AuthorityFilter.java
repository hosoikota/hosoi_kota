package hosoi_kota.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import hosoi_kota.beans.User;

@WebFilter(urlPatterns={"/management", "/signup", "/userediting"})
public class AuthorityFilter implements Filter {

	@Override
	public void destroy() {
		// TODO 自動生成されたメソッド・スタブ

	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
			throws IOException, ServletException {
		HttpSession session = ((HttpServletRequest) req).getSession();

		User user = new User();
		user = (User) session.getAttribute("loginUser");

		if(!(user.getBranchId() == 1 && user.getPositionId() == 1)){
			List<String> message = new ArrayList<String>();
			message.add("権限を与えられていません");
			session.setAttribute("errorMessages", message);
			((HttpServletResponse) resp).sendRedirect("./");
			return;
		}

		chain.doFilter(req, resp);
	}

	@Override
	public void init(FilterConfig config) throws ServletException {
		// TODO 自動生成されたメソッド・スタブ

	}

}
