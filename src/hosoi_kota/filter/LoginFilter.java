package hosoi_kota.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter("/*")
public class LoginFilter implements Filter {

	@Override
	public void destroy() {
		// TODO 自動生成されたメソッド・スタブ

	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
			throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest) req).getSession();
		String path = ((HttpServletRequest) req).getServletPath();
		System.out.println(path);

		if(!path.equals("/css/loginStyle.css")){
		if(!path.equals("/login")){
			if(session == null){ //セッションがなければ初めてアクセスするわけだからログイン画面へ
				((HttpServletResponse) resp).sendRedirect("./login");
				return;
			} else { //セッションがあり、ログインしていなければログイン画面へ
				if(session.getAttribute("loginUser") == null){
					List<String> message = new ArrayList<String>();
					message.add("ログインしてください");
					session.setAttribute("errorMessages", message);
					((HttpServletResponse) resp).sendRedirect("./login");
					return;
				}
			}
		}
		}
		chain.doFilter(req, resp);
	}

	@Override
	public void init(FilterConfig config) throws ServletException {
		// TODO 自動生成されたメソッド・スタブ

	}

}
