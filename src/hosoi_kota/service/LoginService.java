package hosoi_kota.service;

import static hosoi_kota.utils.CloseableUtil.*;
import static hosoi_kota.utils.DBUtil.*;

import java.sql.Connection;

import hosoi_kota.beans.User;
import hosoi_kota.dao.UserDao;
import hosoi_kota.utils.CipherUtil;

public class LoginService {

	public User login(String loginId, String password) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			//パスワード解読してんのかな？
			String encPassword = CipherUtil.encrypt(password);
			//該当ユーザーのパラメータ取得
			User user = userDao.getUser(connection, loginId, encPassword);

			//変更更新
			commit(connection);
			//該当ユーザーを帰す
			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}

}
