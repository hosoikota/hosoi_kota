package hosoi_kota.service;

import static hosoi_kota.utils.CloseableUtil.*;
import static hosoi_kota.utils.DBUtil.*;

import java.sql.Connection;

import hosoi_kota.beans.User;
import hosoi_kota.dao.UserDao;

public class EditingService {
	public User Editing(int editingId) {
		Connection connection = null;
		try {
			connection = getConnection();

			User user = new User();
			UserDao userDao = new UserDao();
			user = userDao.getEditingUser(connection, editingId);

			commit(connection);
			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public boolean selectEditing(int editingId) {
		Connection connection = null;
		try {
			connection = getConnection();
			UserDao userDao = new UserDao();

			commit(connection);
			return userDao.selectEditingUser(connection, editingId);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
