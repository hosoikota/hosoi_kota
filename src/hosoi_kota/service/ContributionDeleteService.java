package hosoi_kota.service;

import static hosoi_kota.utils.CloseableUtil.*;
import static hosoi_kota.utils.DBUtil.*;

import java.sql.Connection;

import hosoi_kota.dao.ContributionDeleteDao;


public class ContributionDeleteService {
	public void delete(int contributionId){
		Connection connection = null;
		try {
			connection = getConnection();

		    ContributionDeleteDao contributionDeleteDao = new ContributionDeleteDao();
			contributionDeleteDao.delete(connection, contributionId);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
