package hosoi_kota.service;

import static hosoi_kota.utils.CloseableUtil.*;
import static hosoi_kota.utils.DBUtil.*;

import java.sql.Connection;

import hosoi_kota.dao.CommentDeleteDao;


public class CommentDeleteService {

	public void delete(int commentId){
		Connection connection = null;
		try {
			connection = getConnection();

			CommentDeleteDao commentDeleteDao = new CommentDeleteDao();
			commentDeleteDao.delete(connection, commentId);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
