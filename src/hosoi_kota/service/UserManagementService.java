package hosoi_kota.service;

import static hosoi_kota.utils.CloseableUtil.*;
import static hosoi_kota.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import hosoi_kota.beans.UserManagement;
import hosoi_kota.dao.UserManagementDao;


public class UserManagementService {

	public List<UserManagement> getUserManagementList() {
		Connection connection = null;
		try {
			connection = getConnection();

			UserManagementDao managementDao = new UserManagementDao();
			List<UserManagement> ret = managementDao.getUserManagementList(connection);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}

	public void updateIsStoped(int userId, int isStoped){
		Connection connection = null;
		try {
			connection = getConnection();
			new UserManagementDao().updateIsStoped(connection, userId, isStoped);;

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
