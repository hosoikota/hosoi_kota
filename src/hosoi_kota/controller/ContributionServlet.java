package hosoi_kota.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import hosoi_kota.beans.Contribution;
import hosoi_kota.beans.User;
import hosoi_kota.service.ContributionService;

@WebServlet(urlPatterns = { "/contribution" })
public class ContributionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		req.getRequestDispatcher("contribution.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		List<String> messages = new ArrayList<String>();
		HttpSession session = req.getSession();

		if(isValid(req, messages) == true){
			User user = (User) session.getAttribute("loginUser");

			Contribution contribution = new Contribution();
			contribution.setSubject(req.getParameter("subject"));
			contribution.setText(req.getParameter("text"));
			contribution.setCategory(req.getParameter("category"));
			contribution.setUserId(user.getId());

			new ContributionService().register(contribution);

			resp.sendRedirect("./");
		} else {
			Contribution contribution = new Contribution();
			contribution.setSubject(req.getParameter("subject"));
			contribution.setCategory(req.getParameter("category"));
			contribution.setText(req.getParameter("text"));
			session.setAttribute("contribution", contribution);
			session.setAttribute("errorMessages", messages);
			resp.sendRedirect("./contribution");
		}
	}

	private boolean isValid(HttpServletRequest req, List<String> messages){

		String subject = req.getParameter("subject");
		String text = req.getParameter("text");
		String category = req.getParameter("category");

		if(StringUtils.isEmpty(subject) == true){
			messages.add("件名が入力されていません");
		}
		if(subject.length() > 30){
			messages.add("件名の入力定義に従っていません");
		}
		if(StringUtils.isEmpty(category) == true){
			messages.add("カテゴリーが入力されていません");
		}
		if(category.length() > 10){
			messages.add("カテゴリーの入力定義に従っていません");
		}
		if(StringUtils.isEmpty(text)){
			messages.add("本文が入力されていません");
		}
		if(text.length() > 1000){
			messages.add("本文が入力定義に従っていません");
		}

		if(messages.size() == 0){
			return true;
		} else {
			return false;
		}
	}

}
