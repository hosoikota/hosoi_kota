package hosoi_kota.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import hosoi_kota.beans.User;
import hosoi_kota.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	 private static final long serialVersionUID = 1L;

	 @Override
	 protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		 req.getRequestDispatcher("signup.jsp").forward(req, resp);
	 }

	 @Override
	 protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		 //エラーメッセージ格納場所
		List<String> messages = new ArrayList<String>();
		HttpSession session = req.getSession();

		if(isValid(req, messages) == true){
			User user = new User();
			user.setLoginId(req.getParameter("login_id"));
			user.setPassword(req.getParameter("password"));
			user.setUserName(req.getParameter("user_name"));
			user.setBranchId(Integer.parseInt(req.getParameter("branch_id")));
			user.setPositionId(Integer.parseInt(req.getParameter("position_id")));

			new UserService().register(user);

			resp.sendRedirect("management");
		} else {
			User user = new User();
			user.setLoginId(req.getParameter("login_id"));
			user.setUserName(req.getParameter("user_name"));
			session.setAttribute("user", user);
			session.setAttribute("errorMessages", messages);
			resp.sendRedirect("signup");
		}
	 }

	 private boolean isValid(HttpServletRequest req, List<String> messages) {

		String loginId = req.getParameter("login_id");
		String password = req.getParameter("password");
		String password2 = req.getParameter("password2");
		String userName = req.getParameter("user_name");

		if(StringUtils.isEmpty(loginId) == true){
			messages.add("ログインIDが入力されていません");
		} else if(loginId.matches("[a-zA-Z0-9]{6,20}") == false){
			messages.add("ログインID入力定義に従っていません");
		} else if(new UserService().signup(loginId) == false){
			messages.add("すでにそのログインIDは使われています");
		}
		if(StringUtils.isEmpty(password) == true){
			messages.add("パスワードが入力されていません");
		} else if(password.matches("[ -~]{6,20}") == false){
			messages.add("パスワード入力定義に従っていません");
		} else if(password.equals(password2) == false) {
			messages.add("パスワードが確認用と一致しません");
		}
		if(StringUtils.isEmpty(userName) == true){
			messages.add("ユーザー名が入力されていません");
		}
		if(userName.length() > 10){
			messages.add("ユーザー名の入力定義に従っていません");
		}

		if(messages.size() == 0 ){
			return true;
		} else {
			return false;
		}
	 }

}
