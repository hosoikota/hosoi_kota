package hosoi_kota.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import hosoi_kota.beans.User;
import hosoi_kota.service.LoginService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		req.getRequestDispatcher("/login.jsp").forward(req,resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		HttpSession session = req.getSession();

		String loginId = req.getParameter("loginId");
		String password = req.getParameter("password");

		LoginService loginService = new LoginService();
		User user = loginService.login(loginId, password);

		if(user != null) {
			if(user.getIsStoped() == 1){
				List<String> message = new ArrayList<String>();
				message.add("アカウントが停止されています");
				session.setAttribute("errorMessages", message);
				session.setAttribute("id", loginId);
				req.getRequestDispatcher("/login.jsp").forward(req,resp);
			}
			session.setAttribute("loginUser", user);
			resp.sendRedirect("./");
		} else {
			List<String> message = new ArrayList<String>();
			message.add("ログインできません");
			session.setAttribute("id", loginId);
			session.setAttribute("errorMessages", message);
			req.getRequestDispatcher("/login.jsp").forward(req,resp);
		}
	}
}
