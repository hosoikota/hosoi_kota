package hosoi_kota.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import hosoi_kota.beans.Comment;
import hosoi_kota.beans.User;
import hosoi_kota.beans.UserComment;
import hosoi_kota.beans.UserContribution;
import hosoi_kota.service.CommentDeleteService;
import hosoi_kota.service.CommentService;
import hosoi_kota.service.ContributionService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

		User user = (User) req.getSession().getAttribute("loginUser");
		HttpSession session = req.getSession();

		String start = req.getParameter("datestart");
		String end =  req.getParameter("dateend");
		String category = req.getParameter("category");

		List<UserContribution> contributions = new ContributionService().getContribution(start, end, category);
		List<UserComment> comments = new CommentService().getComment();

		req.setAttribute("comments", comments);
		req.setAttribute("contributions", contributions);
		req.setAttribute("loginUser", user);
		session.setAttribute("datestart", start);
		session.setAttribute("dateend", end);
		session.setAttribute("category", category);

		req.getRequestDispatcher("/home.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = req.getSession();

		if(req.getParameter("commentId") != null){
			int comment = Integer.parseInt(req.getParameter("commentId"));
			new CommentDeleteService().delete(comment);

			String start = (String) session.getAttribute("datestart");
			String end =  (String) session.getAttribute("dateend");
			String category = (String) session.getAttribute("category");

			List<UserContribution> contributions = new ContributionService().getContribution(start, end, category);
			List<UserComment> comments = new CommentService().getComment();


			req.setAttribute("comments", comments);
			req.setAttribute("contributions", contributions);
			req.getRequestDispatcher("/home.jsp").forward(req, resp);
		} else if(isValid(req, messages) == true){
			Comment comment = new Comment();
			comment.setText(req.getParameter("text"));
			comment.setUserId(Integer.parseInt(req.getParameter("userId")));
			comment.setContributionId(Integer.parseInt(req.getParameter("contributionId")));

			//コメント獲得
			new CommentService().register(comment);

			String start = (String) session.getAttribute("datestart");
			String end =  (String) session.getAttribute("dateend");
			String category = (String) session.getAttribute("category");

			List<UserContribution> contributions = new ContributionService().getContribution(start, end, category);
			List<UserComment> comments = new CommentService().getComment();


			req.setAttribute("comments", comments);
			req.setAttribute("contributions", contributions);
			req.getRequestDispatcher("/home.jsp").forward(req, resp);
		} else {
			String text = req.getParameter("text");
			int contributionId = Integer.parseInt(req.getParameter("contributionId"));
			session.setAttribute("text", text);
			session.setAttribute("id", contributionId);
			session.setAttribute("errorMessages", messages);
			resp.sendRedirect("./");
		}
	}

	private boolean isValid(HttpServletRequest req, List<String> messages){
		String text = req.getParameter("text");

		if(StringUtils.isBlank(text) == true){
			messages.add("コメントが入力されていません");
		}
		if(text.length() > 500){
			messages.add("コメントの入力定義に従っていません");
		}

		if(messages.size() == 0){
			return true;
		} else {
			return false;
		}
	}

}
