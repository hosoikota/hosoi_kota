package hosoi_kota.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import hosoi_kota.beans.User;
import hosoi_kota.beans.UserManagement;
import hosoi_kota.service.UserManagementService;

@WebServlet(urlPatterns = { "/management" })
public class ManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

		HttpSession session = req.getSession();

		List<UserManagement> userManagements = new UserManagementService().getUserManagementList();
		req.setAttribute("userManagements", userManagements);

		User user = (User) req.getSession().getAttribute("loginUser");
		session.setAttribute("nowUser", user);

		req.getRequestDispatcher("management.jsp").forward(req, resp);


	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

		int isStoped = Integer.parseInt(req.getParameter("isStoped"));
		int userId = Integer.parseInt(req.getParameter("userId"));

		HttpSession session = req.getSession();

		new UserManagementService().updateIsStoped(userId, isStoped);

		List<UserManagement> userManagements = new UserManagementService().getUserManagementList();
		req.setAttribute("userManagements", userManagements);

		User user = (User) req.getSession().getAttribute("loginUser");
		session.setAttribute("nowUser", user);

		req.getRequestDispatcher("management.jsp").forward(req, resp);
	}

}
