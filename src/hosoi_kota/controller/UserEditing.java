package hosoi_kota.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import hosoi_kota.beans.User;
import hosoi_kota.service.UserService;

@WebServlet(urlPatterns={ "/userediting" })
public class UserEditing extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = req.getSession();

		if(isValid(req, messages) == true){
			User user = new User();
			user.setId(Integer.parseInt(req.getParameter("editingId")));
			user.setLoginId(req.getParameter("loginId"));
			//パスワード欄が空ならもともとのパスワードを使う
			if(StringUtils.isEmpty(req.getParameter("password")) == false){
				user.setPassword(req.getParameter("password"));
			}
			user.setUserName(req.getParameter("userName"));
			user.setBranchId(Integer.parseInt(req.getParameter("branchId")));
			user.setPositionId(Integer.parseInt(req.getParameter("positionId")));

			new UserService().update(user);

			resp.sendRedirect("management");
		} else {
			User user = new User();
			user.setId(Integer.parseInt(req.getParameter("editingId")));
			user.setLoginId(req.getParameter("loginId"));
			//パスワード欄が空ならもともとのパスワードを使う
			if(StringUtils.isEmpty(req.getParameter("password")) == false){
				user.setPassword(req.getParameter("password"));
			}
			user.setUserName(req.getParameter("userName"));
			user.setBranchId(Integer.parseInt(req.getParameter("branchId")));
			user.setPositionId(Integer.parseInt(req.getParameter("positionId")));
			session.setAttribute("afterEditing", user);
			session.setAttribute("errorMessages", messages);
			req.setAttribute("editingId", user.getId());
			resp.sendRedirect("editing");
		}
	}

	private boolean isValid(HttpServletRequest req, List<String> messages) {

		HttpSession session = req.getSession();
		User user = (User) session.getAttribute("editing");
		String loginId = req.getParameter("loginId");
		String password = req.getParameter("password");
		String password2 = req.getParameter("password2");
		String userName = req.getParameter("userName");
		int branchId = Integer.parseInt(req.getParameter("branchId"));
		int positionId = Integer.parseInt(req.getParameter("positionId"));

		if(StringUtils.isEmpty(loginId) == true){
			messages.add("ログインIDが入力されていません");
		} else if(loginId.matches("[a-zA-Z0-9]{6,20}") == false){
			messages.add("ログインID入力定義に従っていません");
		} else if(loginId.equals(user.getLoginId()) == false && new UserService().signup(loginId) == false){
			messages.add("このログインIDは使われています");
		}
		if(StringUtils.isEmpty(password) == false){
			if(password.matches("[ -~]{6,20}") == false){
				messages.add("パスワード入力定義に従っていません");
			} else if(password.equals(password2) == false) {
				messages.add("パスワードが確認用と一致しません");
			}
		}
		if(StringUtils.isEmpty(userName) == true){
			messages.add("ユーザー名が入力されていません");
		}
		if(userName.length() > 10){
			messages.add("ユーザー名の入力定義に従っていません");
		}
		if(branchId == 1 && 3 <= positionId){
			messages.add("本社にその部署・役職は存在しません");
		}
		if(branchId == 2 && 2 >= positionId){
			messages.add("支店Aにその部署・役職は存在しません");
		}

		if(messages.size() == 0 ){
			return true;
		} else {
			return false;
		}
	}

}
