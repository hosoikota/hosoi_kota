package hosoi_kota.dao;

import static hosoi_kota.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import hosoi_kota.exception.SQLRuntimeException;

public class ContributionDeleteDao {
	public void delete(Connection connection, int contributionId){
		PreparedStatement ps =  null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE ");
			sql.append("FROM contributions ");
			sql.append("WHERE contributions.id = ");
			sql.append("?;");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, contributionId);

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}
