package hosoi_kota.dao;

import static hosoi_kota.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import hosoi_kota.beans.Contribution;
import hosoi_kota.exception.SQLRuntimeException;

public class ContributionDao {

	public void insert(Connection connection, Contribution contribution){
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO contributions ( ");
			sql.append("subject");
			sql.append(", text");
			sql.append(", category");
			sql.append(", user_id"); //userIdってusersテーブルのidがほしい
			sql.append(", create_date");
			sql.append(") VALUES ( ");
			sql.append("?"); //subject、件名
			sql.append(", ?"); //text、本文
			sql.append(", ?"); //category、カテゴリー
			sql.append(", ?"); //userIdってusersテーブルのidがほしい
			sql.append(", CURRENT_TIMESTAMP");
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, contribution.getSubject());
			ps.setString(2, contribution.getText());
			ps.setString(3, contribution.getCategory());
			ps.setInt(4, contribution.getUserId());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}
