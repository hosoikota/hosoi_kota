package hosoi_kota.dao;

import static hosoi_kota.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import hosoi_kota.beans.User;
import hosoi_kota.exception.SQLRuntimeException;

public class UserDao {
	//データベースに登録するメソッド
	public void insert(Connection connection, User user){
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
            sql.append("login_id");
            sql.append(", password");
            sql.append(", user_name");
            sql.append(", branch_id");
            sql.append(", position_id");
           // sql.append(", is_stoped");
            sql.append(", create_date");
            sql.append(") VALUES (");
            sql.append("?"); // user_id
            sql.append(", ?"); // password
            sql.append(", ?"); // user_name
            sql.append(", ?"); // branch_id
            sql.append(", ?"); // pasition_id
            sql.append(", CURRENT_TIMESTAMP"); // create_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLoginId());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getUserName());
            ps.setInt(4, user.getBranchId());
            ps.setInt(5, user.getPositionId());
            ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void update(Connection connection, User user) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET ");
			sql.append("login_id = ?, ");
			if(user.getPassword() != null){
				sql.append("password = ?, ");
			}
			sql.append("user_name = ?, ");
			sql.append("branch_id = ?, ");
			sql.append("position_id = ?, ");
			sql.append("create_date = CURRENT_TIMESTAMP ");
			sql.append("WHERE ");
			sql.append("id = ?;");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLoginId());
			if(user.getPassword() != null){
				ps.setString(2, user.getPassword());
				ps.setString(3, user.getUserName());
				ps.setInt(4, user.getBranchId());
				ps.setInt(5, user.getPositionId());
				ps.setInt(6, user.getId());
			} else {
				ps.setString(2, user.getUserName());
				ps.setInt(3, user.getBranchId());
				ps.setInt(4, user.getPositionId());
				ps.setInt(5, user.getId());
			}


            ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

//	public void updateIsStoped(Connection connection, User user, int isStoped){
//		PreparedStatement ps = null;
//		try {
//			StringBuilder sql = new StringBuilder();
//			sql.append("UPDATE users SET ");
//			sql.append("is_stoped = ? ");
//			sql.append("WHERE ");
//			sql.append("id = ?;");
//
//			ps = connection.prepareStatement(sql.toString());
//
//			ps.setInt(1, isStoped);
//			ps.setInt(2, user.getId());
//
//			ps.executeUpdate();
//		} catch (SQLException e) {
//			throw new SQLRuntimeException(e);
//		} finally {
//			close(ps);
//		}
//	}


	//データベースの情報を取得するメソッド
	public User getUser(Connection connection, String loginId, String password){

		//データベースにSQL文を送るための箱
		PreparedStatement ps = null;

		try {
			//usersテーブルのloginとpasswordをとってくる？
			String sql = "SELECT * FROM hosoi_kota.users WHERE login_id = ? AND password = ?";

			//パラメータつきのSQL文をDBに送るためのpreparedStatementオブジェクト生成したものをpsに入れてる
			ps = connection.prepareStatement(sql);
			//？に順番にどのパラメータを設定してる
			ps.setString(1, loginId);
			ps.setString(2, password);

			//SELECT文を実行するのでexecuteQuery()を使うらしい
			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if(userList.isEmpty() == true) {
				//アカウントがない
				return null;
			} else if (2 <= userList.size()){
				//該当アカウントが複数ならなんかエラーはいてる
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				//該当アカウントの取得
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	//ログインIDが一致しているならだめ！！
	public boolean getUserLoginId(Connection connection, String loginId){

		//データベースにSQL文を送るための箱
		PreparedStatement ps = null;

		try {
			//usersテーブルのloginとpasswordをとってくる？
			String sql = "SELECT * FROM hosoi_kota.users WHERE login_id = ?";

			//パラメータつきのSQL文をDBに送るためのpreparedStatementオブジェクト生成したものをpsに入れてる
			ps = connection.prepareStatement(sql);
			//？に順番にどのパラメータを設定してる
			ps.setString(1, loginId);

			//SELECT文を実行するのでexecuteQuery()を使うらしい
			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if(userList.isEmpty() == true) {
				//アカウントがない
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//idとpasswordが一致するuserをリストにいれてそれを返す
	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();

		try {
			while (rs.next()) {
				//該当レコードのパラメータを取得
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
				String userName = rs.getString("user_name");
				int branchId = rs.getInt("branch_id");
				int positionId = rs.getInt("position_id");
				int isStoped = rs.getInt("is_stoped");
				Timestamp createDate = rs.getTimestamp("create_date");

				//取得したパラメータをuserにセット
				User user = new User();
				user.setId(id);
				user.setLoginId(loginId);
				user.setPassword(password);
				user.setUserName(userName);
				user.setBranchId(branchId);
				user.setPositionId(positionId);
				user.setIsStoped(isStoped);
				user.setCreateDate(createDate);


				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public User getEditingUser(Connection connection, int editingId){
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM hosoi_kota.users WHERE id = ?";
			ps = connection.prepareStatement(sql);
			ps.setInt(1, editingId);

			ResultSet rs = ps.executeQuery();
			return toEditingUser(rs);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private User toEditingUser(ResultSet rs) throws SQLException {
		User user = new User();

		try {
			while(rs.next()){
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
				String userName = rs.getString("user_name");
				int branchId = rs.getInt("branch_id");
				int positionId = rs.getInt("position_id");
				int isStoped = rs.getInt("is_stoped");
				Timestamp createDate = rs.getTimestamp("create_date");

				user.setId(id);
				user.setLoginId(loginId);
				user.setPassword(password);
				user.setUserName(userName);
				user.setBranchId(branchId);
				user.setPositionId(positionId);
				user.setIsStoped(isStoped);
				user.setCreateDate(createDate);
			}
			return user;
		} finally {
			close(rs);
		}
	}

	public boolean selectEditingUser(Connection connection, int editingId){
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM hosoi_kota.users WHERE id = ?";
			ps = connection.prepareStatement(sql);
			ps.setInt(1, editingId);

			ResultSet rs = ps.executeQuery();
			if(rs.next() == true){
				return true; //要素あり
			} else{
				return false; //要素なし
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}
