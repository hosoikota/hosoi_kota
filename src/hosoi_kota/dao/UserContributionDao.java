package hosoi_kota.dao;

import static hosoi_kota.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import hosoi_kota.beans.UserContribution;
import hosoi_kota.exception.SQLRuntimeException;


public class UserContributionDao {

	public List<UserContribution> getUserContributions(Connection connection, int num, String start, String end, String category){
		PreparedStatement ps =  null;
		Date dt = new Date(); //現在日時の取得
		String str = new SimpleDateFormat("yyyy-MM-dd 23:59:59").format(dt);

		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("contributions.id as id, ");
			sql.append("contributions.subject as subject, ");
			sql.append("contributions.category as category, ");
			sql.append("contributions.text as text, ");
			sql.append("users.user_name as userName, ");
			sql.append("users.id as userId, ");
			sql.append("contributions.create_date as createDate ");
			sql.append("FROM contributions ");
			sql.append("INNER JOIN users ");
			sql.append("ON contributions.user_id = users.id ");
			sql.append("WHERE contributions.create_date BETWEEN ? AND ? ");
			//カテゴリーがnullかつ空文字
			if(StringUtils.isEmpty(category) == false){
				sql.append("AND contributions.category LIKE ? ");
			}
			sql.append("ORDER BY contributions.create_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());


			if(StringUtils.isEmpty(start) == true && StringUtils.isEmpty(end) == true){
				ps.setString(1, "2018-05-18 00:00:00");
				ps.setString(2, str);
			}
			if(StringUtils.isEmpty(start) == true && StringUtils.isEmpty(end) == false){
				ps.setString(1, "2018-05-18 00:00:00");
				ps.setString(2, end + " 23:59:59");
			}
			if(StringUtils.isEmpty(start) == false && StringUtils.isEmpty(end) == true){
				ps.setString(1, start);
				ps.setString(2, str);
			}
			if(StringUtils.isEmpty(start) == false && StringUtils.isEmpty(end) == false){
				ps.setString(1, start);
				ps.setString(2, end + " 23:59:59");
			}

			if(StringUtils.isEmpty(category) == false){
				ps.setString(3, "%" + category + "%");
			}

			ResultSet rs = ps.executeQuery();
			List<UserContribution> ret = toUserContributionList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	private List<UserContribution> toUserContributionList(ResultSet rs) throws SQLException {
		List<UserContribution> ret = new ArrayList<UserContribution>();
		try {
			while(rs.next()) {
				int id = rs.getInt("id"); //投稿番号かな
				String subject = rs.getString("subject"); //件名
				String category = rs.getString("category"); //カテゴリー
				String text = rs.getString("text"); //本文
				String userName = rs.getString("userName"); //投稿したユーザー名
				int userId = rs.getInt("userId");
				Timestamp createDate = rs.getTimestamp("createDate");

				UserContribution contribution = new UserContribution();
				contribution.setId(id);
				contribution.setSubject(subject);
				contribution.setCategory(category);
				contribution.setText(text);
				contribution.setUserName(userName);
				contribution.setUserId(userId);
				contribution.setCreateDate(createDate);

				ret.add(contribution);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}
