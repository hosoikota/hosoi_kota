package hosoi_kota.dao;

import static hosoi_kota.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import hosoi_kota.exception.SQLRuntimeException;

public class CommentDeleteDao {

	public void delete(Connection connection, int commentId){
		PreparedStatement ps =  null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE ");
			sql.append("FROM comments ");
			sql.append("WHERE comments.id = ");
			sql.append("?;");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, commentId);

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}
