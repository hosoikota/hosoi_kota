package hosoi_kota.dao;

import static hosoi_kota.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import hosoi_kota.beans.Position;
import hosoi_kota.exception.SQLRuntimeException;

public class PositionDao {

	public List<Position> getPosition(Connection connection){
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM positions ";

			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			List<Position> position = getPositionList(rs);

			return position;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Position> getPositionList(ResultSet rs) throws SQLException {
		List<Position> ret = new ArrayList<Position>();
		try{
			while(rs.next()){
				int id = rs.getInt("id");
				String positionName = rs.getString("position_name");

				Position position = new Position();
				position.setId(id);
				position.setPositionName(positionName);

				ret.add(position);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}
