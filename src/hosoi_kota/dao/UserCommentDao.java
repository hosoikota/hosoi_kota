package hosoi_kota.dao;

import static hosoi_kota.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import hosoi_kota.beans.UserComment;
import hosoi_kota.exception.SQLRuntimeException;


public class UserCommentDao {
	public List<UserComment> getUserComment(Connection connection, int num){
		PreparedStatement ps =  null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("comments.id as id, ");
			sql.append("comments.text as text, ");//コメントのとこ
			sql.append("users.id as userId, ");
			sql.append("users.user_name as userName, ");
			sql.append("comments.contribution_id as contributionId, ");
			sql.append("comments.create_date as createDate ");
			sql.append("FROM comments ");
			sql.append("INNER JOIN users ");
			sql.append("ON comments.user_id = users.id ");
			sql.append("ORDER BY comments.create_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserComment> ret = toUserCommentList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserComment> toUserCommentList(ResultSet rs) throws SQLException {
		List<UserComment> ret = new ArrayList<UserComment>();
		try {
			while(rs.next()) {
				int id = rs.getInt("id"); //投稿番号かな
				String text = rs.getString("text"); //件名
				int userId = rs.getInt("userId");
				String userName = rs.getString("userName"); //カテゴリー
				int contributionId = rs.getInt("contributionId");
				Timestamp createDate = rs.getTimestamp("createDate");

				UserComment comment = new UserComment();
				comment.setId(id);
				comment.setComment(text);
				comment.setUserId(userId);
				comment.setUserName(userName);
				comment.setContributionId(contributionId);
				comment.setCreateDate(createDate);

				ret.add(comment);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}
