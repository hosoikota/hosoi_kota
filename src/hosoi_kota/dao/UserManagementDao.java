package hosoi_kota.dao;

import static hosoi_kota.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import hosoi_kota.beans.UserManagement;
import hosoi_kota.exception.SQLRuntimeException;

public class UserManagementDao {
	public List<UserManagement> getUserManagementList(Connection connection){
		PreparedStatement ps =  null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.id as id, ");
			sql.append("users.login_id as loginId, ");
			sql.append("users.user_name as userName, ");
			sql.append("branches.branch_name as branchName, ");
			sql.append("positions.position_name as positionName, ");
			sql.append("users.is_stoped as isStoped ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ");
			sql.append("ON users.branch_id = branches.id ");
			sql.append("INNER JOIN positions ");
			sql.append("ON users.position_id = positions.id ");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserManagement> ret = toUserManagementList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserManagement> toUserManagementList(ResultSet rs) throws SQLException {
		List<UserManagement> ret = new ArrayList<UserManagement>();
		try {
			while(rs.next()) {
				int id = rs.getInt("id"); //投稿番号かな
				String loginId = rs.getString("loginId"); //件名
				String userName = rs.getString("userName"); //カテゴリー
				String branchName = rs.getString("branchName");
				String positionName = rs.getString("positionName");
				int isStoped = rs.getInt("isStoped");

				UserManagement management = new UserManagement();
				management.setId(id);
				management.setLoginId(loginId);
				management.setUserName(userName);
				management.setBranchName(branchName);
				management.setPositionName(positionName);
				management.setIsStoped(isStoped);

				ret.add(management);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public void updateIsStoped(Connection connection, int userId, int isStoped){
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET ");
			sql.append("is_stoped = ? ");
			sql.append("WHERE ");
			sql.append("id = ?;");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, isStoped);
			ps.setInt(2, userId);

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}
