package hosoi_kota.dao;

import static hosoi_kota.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import hosoi_kota.beans.Branch;
import hosoi_kota.exception.SQLRuntimeException;

public class BranchDao {

	public List<Branch> getBranch(Connection connection){
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM branches ";

			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			List<Branch> branch = getBranchList(rs);

			return branch;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Branch> getBranchList(ResultSet rs) throws SQLException{
		List<Branch> ret = new ArrayList<Branch>();
		try {
			while(rs.next()){
				int id = rs.getInt("id");
				String branchName = rs.getString("branch_name");

				Branch branch = new Branch();
				branch.setId(id);
				branch.setBranchName(branchName);

				ret.add(branch);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}
